
package projectofinal;

public class Usuarios {
    
    private byte[] password;
    private String userName;
    private String socialNet;

    /**
     * @return the password
     */
    public byte[] getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(byte[] password) {
        this.password = password;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the socialNet
     */
    public String getSocialNet() {
        return socialNet;
    }

    /**
     * @param socialNet the socialNet to set
     */
    public void setSocialNet(String socialNet) {
        this.socialNet = socialNet;
    }
    
}
